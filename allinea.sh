#!/bin/bash

#Sincronizzo in db da produzione
#rm mariadb-init/backup.sql
#scp dhwp2@90.147.144.156:/home/dhwp2/backup_migrate/400anni_ateneo/backup-2021-02-06T12-55-11.tar.gz mariadb-init/backup.mysql.gz

#rsync -vrahe ssh dhomeka@90.147.144.145:/home/dhomeka/colodrupal/web/sites/default/files/*  ~/docker/colonizzazioninterne_drupal/web/sites/default/files



docker-compose down;
docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d;

sleep 60
docker exec -ti colodrupal_solr make create core=default -f /usr/local/bin/actions.mk

drush updb -y;
drush cim -y;

drush cr;
drush search-api-clear;
#drush search-api-index;
drush upwd admin admin
