#!/usr/bin/env bash

drush pm-uninstall dh_omeka -y; drush pm-uninstall migrate_tools -y; drush pm-uninstall migrate_plus -y; drush pm-uninstall migrate -y; drush pm-uninstall migrate_source_csv -y

drush en dh_omeka migrate_source_csv -y
