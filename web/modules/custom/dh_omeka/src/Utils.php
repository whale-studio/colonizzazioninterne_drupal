<?php

namespace Drupal\dh_omeka;

class Utils {

  function __construct() {
    $this->url = 'https://storia.dh.unica.it/risorse/api/items/';
  }

  /**
   *$omeka = \Drupal::service('dh_omeka.utils');
   */

  public function getItem($id) {
    $omeka_item_source = file_get_contents($this->url. $id);
    $omeka_item = json_decode($omeka_item_source);
    return $omeka_item;
  }

  public function getDescription($item) {
    $template = $this->getResourceTemplate($item);
    $description_fields = [
      '2' => 'oad:scopeAndContent',
      '4' => 'oad:scopeAndContent',
      '5' => 'oad:scopeAndContent',
      '6' => 'dcterms:description',
      '7' => 'bibo:content',
    ];

    $description_field = $description_fields[$template];
    $descrizione = $item->{$description_field};
    return $descrizione[0]->{'@value'};
  }

  public function getTitle($item) {
    $title = $item->{'dcterms:title'};
    return $title[0]->{'@value'};
  }

  public function getIdFromEck($entity) {
    $id = $entity->get('field_id')->getValue();
    return $id[0]['value'];
  }

  public function getImage($item, $type = 'large') {
    $medias = $item->{"o:media"};
    if (!empty($medias)) {
      $media_url = $medias[0]->{"@id"};
      $media_source = file_get_contents($media_url);
      $media = json_decode($media_source);
      $image_src = $media->thumbnail_display_urls->{$type};
      return $image_src;
    }
  }

  public function getResourceTemplate($item) {
    $resource_id = $item->{'o:resource_template'};
    return $resource_id->{"o:id"};
  }

  public function getLatLon($item) {
    $marker_url = $item->{'o-module-mapping:marker'};
    $marker_object = file_get_contents($marker_url[0]->{'@id'});
    $marker = json_decode($marker_object);
    $values['lat'] = $marker->{'o-module-mapping:lat'};
    $values['lon'] = $marker->{'o-module-mapping:lng'};
    $values['title'] = $marker->{'o-module-mapping:label'};
    $values['url'] = 'https://storia.dh.unica.it/risorse/s/colonizzazioninterne/item/' . $item->{'o:id'};
    $values['image'] = $this->getImage($item);
    return $values;
  }

  public function getResourceName($resource_id) {
    $resources = [
      '12' => 'Risorsa Biografie',
      '24' => 'Risorsa cartografica',
      '24' => 'Risorsa cartografica',
      '25' => 'Risorsa documentale',
      '8' => 'Risorsa Evento',
      '22' => 'Risorsa fotografica',
      '11' => 'Risorsa link',
      '23' => 'Risorsa opera d\'arte',
      '20' => 'Risorsa Bibliografica',
      '7' => 'Risorsa website',
      '32' => 'Risorsa - Mappa Archivistica',
      '27' => 'Risorsa Audiovideo',
      '35' => 'Risorsa Collezione',
      '39' => 'Risorsa Colonie | colonizzazioni interne',
      '31' => 'Risorsa demoetnoantropologica immateriale',
      '30' => 'Risorsa demoetnoantropologica materiale',
      '36' => 'Risorsa Descrizione Archivistica',
      '4' => 'Risorsa interviste',
      '38' => 'Risorsa istituzione: Consolato del Mare',
      '42' => 'Risorsa oggetto digitale ASMSA',
      '26' => 'Risorsa periodico a stampa',
      '29' => 'Risorsa persone: schiavi',
      '10' => 'Risorsa piano di lezione',
      '17' => 'Risorsa Software',
      '44' => 'Risorsa Spazio marittimo | ASMSA',
      '37' => 'Risorsa Studio di colonia',
    ];

    return $resources[$resource_id];

  }

}

