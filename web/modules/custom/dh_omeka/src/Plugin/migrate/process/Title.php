<?php

/**
 * @file
 * Contains \Drupal\migrando\Plugin\migrate\process\Encode.
 */
namespace Drupal\dh_omeka\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Process latitude and longitude and return the value for the D8 geofield.
 *
 * @MigrateProcessPlugin(
 *   id = "title"
 * )
 */
class Title extends ProcessPluginBase {
  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return substr($value['@value'], 0, 254);
  }

}
