<?php

namespace Drupal\dh_omeka\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;

/**
 *
 * @SearchApiProcessor(
 *   id = "descrizione",
 *   label = @Translation("Descrizione"),
 *   description = @Translation("Descrizione"),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = false,
 * )
 */
class Descrizione extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Descrizione'),
        'description' => $this->t('Descrizione oggetto omeka'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['search_api_descrizione'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {
    $entity = $item->getOriginalObject()->getValue();
    /* @var \Drupal\node\Entity\Node $entity*/
    /* @var \Drupal\node\Entity\Node $carriera*/
    if ($entity->bundle() == 'omeka') {
      $fields = $this->getFieldsHelper()
        ->filterForPropertyPath($item->getFields(), NULL, 'search_api_descrizione');
      foreach ($fields as $field) {
        if (!$field->getDatasourceId()) {
          $omeka = \Drupal::service('dh_omeka.utils');
          $object = $item->getOriginalObject();
          $nodo = $object->getEntity();
          $omeka_id = $omeka->getIdFromEck($nodo);
          $omeka_item = $omeka->getItem($omeka_id);
          $descrizione = $omeka->getDescription($omeka_item);
          $field->addValue($descrizione[0]->{'@value'});
        }
      }
    }
  }
}
