<?php

use Drupal\block_content\BlockContentInterface;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;

/**
 * Implements hook_preprocess_html()
 */
function archivio_preprocess_html(&$variables) {
  $node = \Drupal::request()->attributes->get('node');
  $term = \Drupal::request()->attributes->get('taxonomy_term');

  $current_uri = \Drupal::request()->getRequestUri();
  $variables['attributes']['class'][] = 'custom-path-' . str_replace('/', '', $current_uri);


  $variables['archivio_container'] = 'container';

  if ($node) {
    $variables['attributes']['class'][] = 'nodo-' . $node->bundle();

    if ($node->bundle() == 'page') {
      $variables['archivio_container'] = 'no-container';
    }
  }

  if ($term) {
    $variables['archivio_container'] = 'no-container';
  }
}

/**
 * Implements hook_preprocess_page()
 */
function archivio_preprocess_media(&$vars) {
  $media = $vars['media'];
  /*if ($vars['view_mode'] == 'galleria') {
    $immagine = $media->get('field_media_image')->getValue();
    $file = \Drupal\file\Entity\File::load($immagine[0]['target_id']);
    $uri = $file->getFileUri();
    $vars['img_source']  = \Drupal\image\Entity\ImageStyle::load('metatag')->buildUrl($uri);
  }*/
}

/**
 * Implements hook_preprocess_page()
 */
function archivio_preprocess_page(&$variables) {
  $variables['ente_appartenenza'] = theme_get_setting('ente_appartenenza');

  $node = \Drupal::request()->attributes->get('node');
  $term = \Drupal::request()->attributes->get('taxonomy_term');

  $variables['archivio_container'] = 'container';

  if ($node) {
    if ($node->bundle() == 'page') {
      $variables['archivio_container'] = 'no-container';
    }
  }

  if ($term) {
    $variables['archivio_container'] = 'no-container';
  }
}

function archivio_preprocess_eck_entity(&$vars) {
  $omeka = \Drupal::service('dh_omeka.utils');
  $entity = $vars['eck_entity'];
  $omeka_id = $omeka->getIdFromEck($entity);
  $omeka_item = $omeka->getItem($omeka_id);
  $vars['omeka_id'] = $omeka_id;
  $vars['descrizione'] = $omeka->getDescription($omeka_item);
  $vars['titolo'] = $omeka->getTitle($omeka_item);
  $vars['immagine'] = $omeka->getImage($omeka_item, 'large');
  $vars['immagine_square'] = $omeka->getImage($omeka_item, 'square');
}

/**
 * Implements hook_preprocess_page()
 */
function archivio_preprocess_field(&$vars) {
  $element = $vars['element'];

  if ($vars['element']['#field_name'] == 'field_data_inizio_carriera') {
    $node = $vars['element']['#object'];
    if ($node->bundle() == 'carriera' && !$node->get('field_tipologia_carriera')
        ->isEmpty()) {
      $tipologia = $node->get('field_tipologia_carriera')->getValue();
      if ($tipologia[0]['value'] == 's') {
        $vars['label'] = 'Anno accademico di immatricolazione';
      }
    }
  }

  if ($vars['element']['#field_name'] == 'field_data_fine_carriera') {
    $node = $vars['element']['#object'];
    if ($node->bundle() == 'carriera' && !$node->get('field_tipologia_carriera')
        ->isEmpty()) {
      $tipologia = $node->get('field_tipologia_carriera')->getValue();
      if ($tipologia[0]['value'] == 's') {
        $vars['label'] = 'Anno accademico di laurea';
      }
    }
  }

  if ($vars['element']['#field_name'] == 'field_omeka_item') {
    $block = $vars['element']['#object'];
    $type = $block->get('type')->getValue();

  }
}

/**
 * Implements hook_theme_suggestions_page_alter().
 * https://www.drupal.org/node/2521876#comment-10684366
 */
function aarchivio_suggestions_page_alter(array &$suggestions, array $variables) {
  // Add content type suggestions.
  if (($node = \Drupal::request()->attributes->get('node')) && (strpos($_SERVER['REQUEST_URI'], "revisions") == FALSE)) {
    array_splice($suggestions, 1, 0, 'page__node__' . $node->getType());
    $variables['content_type_name'] = $node->getType();
  }
}

/**
 * Implements hook_preprocess_HOOK() for block.html.twig.
 */
function archivio_preprocess_block(&$variables) {
  // Add the block ID as custom attribute to block content, this will be used
  // for menu template suggestions.
  if (isset($variables['elements']['#id'])) {
    $variables['content']['#attributes']['block'] = $variables['elements']['#id'];
  }

  if (isset($variables['elements']['#id'])) {
    $variables['content']['#attributes']['block'] = $variables['elements']['#id'];
  }

  $variables['base_path'] = base_path();

  if (isset($variables['elements']['content']['#block_content'])) {
    $block = $variables['elements']['content']['#block_content'];
    if ($variables['plugin_id'] == "inline_block:hero" || $variables['plugin_id'] == "inline_block:homepage_top") {
      if ($block->hasField('field_immagine_sfondo')) {
        if (!$block->get('field_immagine_sfondo')->isEmpty()) {
          $mid = $block->field_immagine_sfondo[0]->getValue()['target_id'];
          $fid = Media::load($mid)->field_media_image[0]->getValue()['target_id'];
          $file = File::load($fid);
          $variables['immagine_sfondo'] = $file->url();
        }
      }
    }
    
    if ($variables['plugin_id'] == "inline_block:omeka_mappa") {
      if (!$block->get('field_wms_link')->isEmpty()) {
        /** Calcolo WMS */
        $wmss = $block->get('field_wms_link')->getValue();
        $wms_layers = [];
        foreach ($wmss as $wms) {
          $values_wms = [
            'url' => $wms['uri'],
            'layer' => $wms['title'],
          ];
          $wms_layers[] = $values_wms;
        }
        $variables['#attached']['drupalSettings']['omeka']['wms'] = $wms_layers;
        $variables['wms_data'] = $wms_layers;
        $variables['block_unique_id'] = \Drupal\Component\Utility\Html::getUniqueId($variables['plugin_id']);
      }

      /** Calcolo oggetti omeka */
      $items = $block->get('field_omeka_item')->referencedEntities();
      $omeka = \Drupal::service('dh_omeka.utils');
      $markers = [];
      foreach ($items as $item) {
        $omeka_id = $omeka->getIdFromEck($item);
        $omeka_item = $omeka->getItem($omeka_id);
        $markers[] = $omeka->getLatLon($omeka_item);
      }

      $variables['#attached']['drupalSettings']['omeka']['items'] = $markers;
      $variables['omeka_items'] = $markers;
    }
  }

  if ($variables['plugin_id'] == 'inline_block:persone') {
    $block = $variables['elements']['content']['#block_content'];
    $tipologia = $block->get('field_tipologia')->getValue();
    $service_name = $tipologia[0]['value'];
    $personas_original = file_get_contents('https://archiviostorico.unica.it/api/persone/' . $service_name . '.json');
    $personas = json_decode($personas_original);

    /** OMEKA TEST */

    /**
     * paginazione:
     *   - page (0 o 1 dà sempre la prima pagina)
     *   - per_page (numero risultati per pagina)
     */

    /** tutti gli oggetto di una singola collezione con titolo e immagine */
    /*$omeka_items = file_get_contents('https://storia.dh.unica.it/storiedigitali/api/items');
    $omeka_items_object = json_decode($omeka_items);
    foreach ($omeka_items_object as $omeka) {
      $media = $omeka->{"o:media"};
      foreach ($media as $media_singolo) {
        //$singolo_media = json_decode(file_get_contents($media_singolo->{"@id"}));
        //$immagine_url = $singolo_media->{"o:original_url"};
      }
    }*/

    /**
     * id, titolo e oggetti di una singola collezione
     */
    /*$item_sets = json_decode(file_get_contents('https://storia.dh.unica.it/storiedigitali/api/item_sets'));
    foreach ($item_sets as $collection) {
      $name = $collection->{"dcterms:title"};
      $collection_id = $collection->{"o:id"};
      $collection_title = $name[0]->{"@value"};
      //$collection_items = json_decode(file_get_contents('https://storia.dh.unica.it/storiedigitali/api/items?item_set_id=' . $collection_id));
    }*/

    /** Ricerca fulltext*/
    /*$omeka_search = json_decode(file_get_contents('https://storia.dh.unica.it/storiedigitali/api/items?fulltext_search=sardegna'));
    foreach ($omeka_search as $omeka) {
      $get_title = $omeka->{"dcterms:title"};
      $title = $get_title[0]->{"@value"};
    }*/

    /**
     *  Ricerca fine sul valore di una proprietà:
     *
     * https://storia.dh.unica.it/storiedigitali/api/items?property[0][property]=8&property[0][type]=in&property[0][text]=Imbarcazione%20a%20vela&property[0][joiner]=and
     */

    /** FINE OMEGA TEST */

    foreach ($personas as $persona) {
      $mandati_rettore = [];
      $daticarrierahonoris = [];
      $daticarrieraillustri = [];
      $daticarrieradonne = [];

      if ($service_name == 'rettori') {
        $fine_mandato = explode(', ', $persona->fine_mandato);
        $inizio_mandato = explode(', ', $persona->inizio_mandato);
        if (count($fine_mandato) > 1 && count($inizio_mandato) > 1) {
          $mandati_rettore = $inizio_mandato[0] . '-' . $fine_mandato[0] . ', ' . $inizio_mandato[1] . '-' . $fine_mandato[1];
        }
      }

      if ($service_name == 'honoris_causae') {
        $daticarrierahonoris = json_decode($persona->daticarrierahonoris[0]);
      }

      if ($service_name == 'docenti_illustri') {
        $daticarrieraillustri = json_decode($persona->daticarrieraillustri[0]);
      }

      if ($service_name == 'prime_donne') {
        $daticarrieradonne = json_decode($persona->daticarrieradonne[0]);
      }

      $variables['persone'][] = [
        'tipologia' => $service_name,
        'titolo' => html_entity_decode($persona->titolo, ENT_QUOTES),
        'luogo_nascita' => $persona->luogo_nascita,
        'url' => $persona->url,
        'data_nascita' => $persona->data_nascita,
        'data_nascita_solo_anno' => $persona->data_nascita_solo_anno,
        'foto' => str_replace([' ', '\n'], '', $persona->foto),
        'carriera' => $persona->carriera,
        'carriera_extra_accademica' => $persona->carriera_extra_accademica,
        'fine_carriera' => $persona->fine_carriera,
        'inizio_carriera' => $persona->inizio_carriera,
        'fine_mandato' => $persona->fine_mandato,
        'inizio_mandato' => $persona->inizio_mandato,
        'facolta' => html_entity_decode($persona->persona_facolta, ENT_QUOTES),
        'corso' => html_entity_decode($persona->corso, ENT_QUOTES),
        'insegnamenti' => html_entity_decode($persona->insegnamenti, ENT_QUOTES),
        'mandati_rettore' => $mandati_rettore,
        'daticarrierahonoris' => $daticarrierahonoris,
        'daticarrieraillustri' => $daticarrieraillustri,
        'daticarrieradonne' => $daticarrieradonne,
      ];

    }
  }
}

/**
 * Implements hook_theme_suggestions_block_alter().
 */
function archivio_theme_suggestions_block_alter(array &$suggestions, array $variables) {
  $content = $variables['elements']['content'];
  if (isset($content['#block_content']) and $content['#block_content'] instanceof BlockContentInterface) {
    $bundle = $content['#block_content']->bundle();
    $view_mode = $content['#view_mode'];
    $suggestions[] = 'block__' . $bundle;
    $suggestions[] = 'block__' . $view_mode;
    $suggestions[] = 'block__' . $bundle . '__' . $view_mode;
  }
}

/**
 * Implements hook_entity_view().
 */
function archivio_preprocess_node(&$vars) {
  $service = \Drupal::service('archivio_utils.utils');
  global $base_url;
  $vars['base_url'] = $base_url;
  $view_mode = $vars['view_mode'];
  /**  @var \Drupal\node\Entity\Node $node * */
  /**  @var \Drupal\node\Entity\Node $carriera * */
  $node = $vars['node'];

  if ($node->bundle() == 'persona' && ($view_mode == 'full' || $view_mode == 'ricerca')) {

    $vars['indirizzo'] = FALSE;
    if (!$node->get('field_indirizzo')->isEmpty()) {
      $indirizzo = $node->get('field_indirizzo')->getValue();
      if (!empty($indirizzo[0]['locality'])) {
        $vars['indirizzo'] = TRUE;
      }
    }

    $vars['data_nascita_normale'] = FALSE;
    $vars['data_nascita_anno'] = FALSE;
    if (!$node->get('field_data_nascita')->isEmpty()) {
      $vars['data_nascita_normale'] = TRUE;
    }
    if (!$node->get('field_data_nascita_anno')->isEmpty()) {
      $vars['data_nascita_anno'] = TRUE;
    }

    $vars['sesso'] = 'm';
    if (!$node->get('field_sesso')->isEmpty()) {
      $genere = $node->get('field_sesso')->getValue();
      $vars['sesso'] = $genere[0]['value'];
    }

    $vars['label_anno'] = "Data nascita";
    /*if ($vars['sesso'] == 'f') {
      $vars['label_anno'] = "Nata";
    }*/

    $author_name = $node->getOwner();
    if (!$author_name->get('field_nome_e_cognome')->isEmpty()) {
      $autore_nome = $author_name->get('field_nome_e_cognome')->getValue();
      $vars['autore'] = $autore_nome[0]['value'];
    }
    else {
      $vars['autore'] = 'Nome dell\'autore';
    }

    $vars['carriere']['docente'] = FALSE;
    $vars['carriere']['studente'] = FALSE;
    $vars['carriere']['rettore'] = FALSE;

    $query = \Drupal::entityQuery('node')
      ->condition('field_persona', $node->id())
      ->condition('status', 1)
      ->condition('type', 'carriera');

    $nids = $query->execute();
    $studente = [];
    $docente = [];
    $rettore = [];
    $facolta_persona = [];
    if (count($nids) > 0) {
      $carriere = \Drupal::entityTypeManager()
        ->getStorage('node')
        ->loadMultiple($nids);

      foreach ($carriere as $carriera) {
        $tipologia = $carriera->get('field_tipologia_carriera')->getValue();
        if ($tipologia[0]['value'] == 's') {
          $studente[] = $service->getIconaCarriera($carriera);
        }
        if ($tipologia[0]['value'] == 'd') {
          $docente[] = $service->getIconaCarriera($carriera);
        }
        if ($tipologia[0]['value'] == 'r') {
          $rettore[] = $service->getIconaCarriera($carriera);
        }

        if (!$carriera->get('field_facolta')->isEmpty()) {
          $facoltas = $carriera->get('field_facolta')->referencedEntities();
          foreach ($facoltas as $facolta) {
            $facolta_persona[] = $facolta->label();
          }
        }
      }
    }

    if (count($studente) > 0) {
      $vars['carriere']['studente'] = $studente;
    }

    if (count($docente) > 0) {
      $vars['carriere']['docente'] = $docente;
    }

    if (count($rettore) > 0) {
      $vars['carriere']['rettore'] = $rettore;
    }

    $vars['foto_default'] = FALSE;
    if ($node->get('field_foto')->isEmpty()) {
      $vars['foto_default'] = TRUE;
    }

    $new_facolta = array_unique($facolta_persona);
    $vars['facolta_persona'] = implode(', ', $new_facolta);

  }

  if ($node->bundle() == 'carriera') {
    $tipologia = $node->get('field_tipologia_carriera')->getValue();
    $vars['laureato'] = FALSE;
    if ($tipologia[0]['value'] == 's') {

      if (!$node->get('field_data_fine_carriera')) {
        $vars['laureato'] = TRUE;
      }
    }

    $vars['revisore'] = FALSE;
    $vars['autore'] = FALSE;

    if (!$node->get('field_revisore')->isEmpty()) {
      $vars['revisore'] = $service->implodeValue($node, 'field_revisore');
    }

    if (!$node->get('field_autore')->isEmpty()) {
      $vars['autore'] = $service->implodeValue($node, 'field_autore');
    }

    $vars['icona_url'] = $service->getIconaCarriera($node);

  }

}

/**
 * Implements hook_theme_suggestions_taxonomy_term_alter().
 */
function archivio_theme_suggestions_taxonomy_term_alter(&$suggestions, &$vars) {
  $suggestions[] = 'taxonomy_term__' . $vars['elements']['#view_mode'];
  $suggestions[] = 'taxonomy_term__' . $vars['elements']['#taxonomy_term']->getVocabularyId();
  $suggestions[] = 'taxonomy_term__' . $vars['elements']['#taxonomy_term']->getVocabularyId() . '__' . $vars['elements']['#view_mode'];
}

function archivio_preprocess_maintenance_page(&$vars) {
  global $base_url;
  $vars['base_url'] = $base_url;
}
