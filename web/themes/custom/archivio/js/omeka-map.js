(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.archivio = {
    attach: function (context, settings) {
      if (context === document) {
        /** Get settings from php **/
        var confs = drupalSettings.omeka;

        // confs.wms => l'url del WMS (in teoria possono essere più di uno, per ora è solo uno)
        // confs.items => array con lat/lon/label per gli oggetti
        console.log(confs)
        var omeka_mappa_number = $('div#omeka-mappa').length;
        console.log(omeka_mappa_number);
        $('div#omeka-mappa').each(function(index) {
          var omekaID = $(this).attr('id');
          $(this).attr('id', omekaID + index);
        });
        for(var k = 0; k < omeka_mappa_number; k++){
          // Mappa su tutta la Sardegna come fallback assenza punti
          var map = L.map('omeka-mappa'+k).setView([40.078072, 9.283447], 7);

          // Layers WMS presi da php
          var basemaps = {};

          // Layer base con tile presi da stamen
          basemaps["Terreno"] = L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/toner-lite/{z}/{x}/{y}{r}.{ext}', {
                  subdomains: 'abcd',
                  minZoom: 0,
                  maxZoom: 18,
                  ext: 'png',
                })
          if ("wms" in confs) {
            for (var i = 0; i < confs.wms.length; i++) {
              var wms = confs.wms[i]
              if (wms.url && wms.layer) {
                basemaps[wms.layer] = L.tileLayer.wms(wms.url.split('?')[0], {
                    layers: wms.layer,
                    // opacity: 0.5,
                    format: 'image/png',
                    transparent: 'true'
                })
              }
            }
          }


          // L.control.layers(basemaps).addTo(map);

          // Il primo layer WMS come visualizzazione di default
          for (var i = 0; i < Object.values(basemaps).length; i++) {
            Object.values(basemaps)[i].addTo(map)
          }

          // Creazione e visualizzazione dei marker, separati in un gruppo
          var markers = L.markerClusterGroup();
          for (var i = 0; i < confs.items.length; i++) {
            var item = confs.items[i]
            if (item.lat && item.lon) {
              var marker = L.marker([item.lat, item.lon], {
                title: item.title
              }).bindPopup('' + '' +
                '<a href="'+item.url+'" target="_blank"><img style="max-width: 100%" src="'+item.image+'" alt=""/>'+item.title+'</a>')
              markers.addLayer(marker);
            }
          }
          map.addLayer(markers);

          // Zoom sul gruppo dei marker
          map.fitBounds(markers.getBounds());
        }
      }
    }
  };
})(jQuery, Drupal);
